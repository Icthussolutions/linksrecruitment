<?php
/**
 * The template for displaying search forms in Underscores.me
 *
 * @package linksrecruitment
 */

?>
<form method="get" id="searchform" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<label class="assistive-text" for="s"><?php esc_html_e( 'Search', 'linksrecruitment' ); ?></label>
	<div class="input-group">
		<input class="field form-control" id="s" name="s" type="search"
			placeholder="<?php esc_attr_e( 'Search &hellip;', 'linksrecruitment' ); ?>" value="<?php the_search_query(); ?>">
		<span class="input-group-append">
			<input class="search-submit submit btn btn-primary" id="searchsubmit" name="submit" type="submit"
			value="<?php esc_attr_e( 'Search', 'linksrecruitment' ); ?>">
	</span>
	</div>
</form>
