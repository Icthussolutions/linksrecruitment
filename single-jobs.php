<?php
/**
 * The template for displaying all single posts.
 *
 * @package linksrecruitment
 */

get_header();
$container   = get_theme_mod( 'linksrecruitment_container_type' );
?>

<div class="wrapper" id="single-wrapper">

    <?php while ( have_posts() ) : the_post(); ?>

    <div class="banner jobs bg-primary">

        <header class="entry-header container">

            <div class="row">

                <div class="col-md-12 text-center">

                    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

                    <ul class="entry-meta">
                        <?php
                            echo get_the_term_list( $post->ID, 'job_type', '<li>Job Type: ', ',', '</li>' );
                            echo get_the_term_list( $post->ID, 'job_hours', '<li>Hours: ', ',', '</li>' );
                            echo get_the_term_list( $post->ID, 'industry', '<li>Industry Sector: ', ',', '</li>' );
                            echo get_the_term_list( $post->ID, 'job_location', '<li>Location: ', ',', '</li>' );
                        ?>
                    </ul><!-- .entry-meta -->

                </div>

            </div>

        </header><!-- .entry-header -->

    </div><!-- .banner -->

    <?php endwhile; ?>

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">
            <div class="col-md-8 col-lg-9">
                <main class="site-main" id="main">

                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php get_template_part( 'loop-templates/content', 'single-jobs' ); ?>

                            <?php linksrecruitment_post_nav(); ?>

                    <?php endwhile; // end of the loop. ?>

                </main><!-- #main -->
            </div>


            <!-- Do the right sidebar check -->
            <?php get_template_part( 'global-templates/right-sidebar-check' ); ?>
        

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
