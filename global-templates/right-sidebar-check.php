<?php
/**
 * Right sidebar check.
 *
 * @package linksrecruitment
 */
?>


<?php $sidebar_pos = get_theme_mod( 'linksrecruitment_sidebar_position' ); ?>

<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

  <?php get_template_part( 'sidebar-templates/sidebar', 'right' ); ?>

<?php endif; ?>
