<?php
/**
 * Template Name: Full Width Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package linksrecruitment
 */

get_header();
$container = get_theme_mod( 'linksrecruitment_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">
<?php if( get_field('banner_image') ) { ?>

	<div class="banner text-center" style="background-image:url('<?php the_field( 'banner_image' ); ?>');">

			<header class="entry-header">

				<h1 class="align-middle"><?php the_field( 'page_title' ); ?> <?php the_field( 'page_title_highlight' ); ?></h1>

			</header><!-- .entry-header -->

	</div>

<?php } else { ?>

	<div class="banner default text-center">

		<header class="entry-header">

			<h1 class="align-middle"><?php the_field( 'page_title' ); ?> <span><?php the_field( 'page_title_highlight' ); ?></span></h1>

		</header><!-- .entry-header -->

	</div>

<?php } ?>

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">

				<main class="site-main" id="main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'fullwidth' ); ?>


					<?php endwhile; // end of the loop. ?>


				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

		<div class="about-section">
			<div class="row">
				<div class="col-md-12 section-header">
					<h2 class="text-center">
						<?php the_field( 'section_title' ); ?>
					</h2>
				</div>
				<div class="col-md-7">
					<?php the_field( 'section_content' ); ?>
					<h3 class="text-center">
						<?php the_field( 'section_subtitle' ); ?>
					</h3>
				</div>
				<div class="col-md-5">
				<?php
					$image = get_field( 'section_image' );
					$size = 'full'; // (thumbnail, medium, large, full or custom size)
					
					if( $image ) {
					
						echo wp_get_attachment_image( $image, $size );
					
					}
				?>
				</div>
			</div>
		</div>
		

	</div><!-- Container end -->



	<section class="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="box">
                        <h2><?php the_field( 'contact_title' ); ?></h2>
				        <a href="/contact" class="btn btn-lg btn-primary"><?php the_field( 'contact_button_text' ); ?></a>
                    </div>
				</div>
			</div>
		</div>
	</section>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
