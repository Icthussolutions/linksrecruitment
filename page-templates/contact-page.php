<?php
/**
 * Template Name: Contact
 *
 *
 * @package linksrecruitment
 */

get_header();
$container = get_theme_mod( 'linksrecruitment_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">
<?php if( get_field('banner_image') ) { ?>

	<div class="banner text-center" style="background-image:url('<?php the_field( 'banner_image' ); ?>');">

			<header class="entry-header">

				<h1 class="align-middle"><?php the_field( 'page_title' ); ?> <?php the_field( 'page_title_highlight' ); ?></h1>

			</header><!-- .entry-header -->

	</div>

<?php } else { ?>

	<div class="banner default text-center">

		<header class="entry-header">

			<h1 class="align-middle"><?php the_field( 'page_title' ); ?> <?php the_field( 'page_title_highlight' ); ?></h1>

		</header><!-- .entry-header -->

	</div>

<?php } ?>

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="contact-section">
			<div class="row">
				<div class="col-md-7">
                <?php 

                    $location = get_field('location');

                    if( !empty($location) ):
                    ?>
                    <div class="acf-map">
                        <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                            <h4><?php the_field( 'head_office' ); ?></h4>
                            <p><?php the_field( 'address' ); ?></p>
                        </div>
                    </div>
                <?php endif; ?>
				</div>
				<div class="col-md-5">
                    <h2><span><?php the_field( 'contact_title_highlight' ); ?></span>&nbsp;<?php the_field( 'contact_title' ); ?></h2>
                    <h3><?php the_field( 'head_office' ); ?></h3>
                    <p><?php the_field( 'address' ); ?></p>
                    <p><i class="fa fa-envelope"></i><a href="mailto:<?php the_field( 'email_address' ); ?>"><?php the_field( 'email_address' ); ?></a></p>
                    <p><i class="fa fa-phone"></i><a href="tel:<?php the_field( 'telephone_no_link' ); ?>"><?php the_field( 'telephone_no_display' ); ?></a></p>
				</div>
			</div>
		</div>
		

    </div><!-- Container end -->

	<section class="bg-primary">

            <?php while ( have_posts() ) : the_post(); ?>

                <?php get_template_part( 'loop-templates/content', 'page' ); ?>

            <?php endwhile; // end of the loop. ?>

	</section>

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<style type="text/css">

.acf-map {
	width: 100%;
	height: 400px;
	border: #ccc solid 1px;
	margin: 20px 0;
}

/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCfhdMHdvA25Hr4kWGLDvD0I8L0MTni-Y"></script>
<script type="text/javascript">
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 20,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>