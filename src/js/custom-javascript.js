jQuery(function ($) {

    // Search toggle
    $('.navbar-nav .search-toggle').click(function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('active').find('input[type="search"]').focus();
    });
    $('.search-submit').click(function (e) {
        if ($(this).parent().find('.search-field').val() == '') {
            e.preventDefault();
            $(this).parent().parent().removeClass('active');
        }

    });

    $(".job-description ul").prev("p").addClass('desc-subtitle');

    $(".wp-video-shortcode").attr({
        muted: true,
        playsinline: true
    })
});
