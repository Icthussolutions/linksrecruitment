<?php
/**
 * The template for displaying jobs archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package linksrecruitment
 */

get_header();
?>

<?php
$container   = get_theme_mod( 'linksrecruitment_container_type' );
?>

<div class="wrapper" id="archive-wrapper">
    <div class="banner jobs bg-primary">
        <header class="page-header text-center">
            <h1>
                <?php
                _e( 'Current Jobs: By Industry', 'linksrecruitment' );
                ?>
            </h1>
        </header><!-- .page-header -->
    </div>
    

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		

		<main class="site-main mt-4" id="main">

            <div class="row">

                <?php if ( have_posts() ) : ?>
                <div class="col-md-4 col-lg-3 mb-4">
                    <div class="card">
                        <div class="card-header bg-primary color-white">
                            <h4>
                            <?php
                            _e( 'Filter Jobs by:', 'linksrecruitment' );
                            ?>
                            </h4>
                        </div>
                        <div class="card-body">
                        <?php echo do_shortcode( '[searchandfilter id="267"]' ); ?>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="col-md-8 col-lg-9">

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'loop-templates/content', 'jobs' );
						?>

                    <?php endwhile; ?>
                    

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

                <?php endif; ?>
                
                </div>

			</main><!-- #main -->
            <div class="row">
                <!-- The pagination component -->
                <div class="col-12 mt-4 text-center">
                    <?php wp_pagenavi(); ?>
                </div>
            </div>
            


	</div> <!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
