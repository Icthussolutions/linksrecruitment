<?php
/**
 * The template for displaying search results pages.
 *
 * @package linksrecruitment
 */

get_header();

$container = get_theme_mod( 'linksrecruitment_container_type' );

?>

<div class="wrapper" id="search-wrapper">
    <div class="banner jobs bg-primary">

        <?php if ( have_posts() ) : ?>

        <header class="page-header text-center">
            
            <h1 class="page-title"><?php printf(
            /* translators:*/
            esc_html__( 'Search Results for: %s', 'linksrecruitment' ),
                '<strong>' . get_search_query() . '</strong>' ); ?></h1>

		</header><!-- .page-header -->

		<?php else : ?>

		<header class="page-header text-center">
            
            <h1 class="page-title">
				<?php  _e( 'Sorry no results found for that query', 'linksrecruitment' ); ?></h1>

		</header><!-- .page-header -->

        <?php endif; ?>
        
    </div>
    

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		

		<main class="site-main mt-4" id="main">

            <div class="row">

                <div class="col-md-3 mb-4">

                    <div class="card">

                        <div class="card-header bg-primary color-white">

                            <h4>
                            <?php
                            _e( 'Filter Jobs by:', 'linksrecruitment' );
                            ?>
							</h4>
							
						</div>
						
                        <div class="card-body">
                        <?php echo do_shortcode( '[searchandfilter id="267"]' ); ?>
						</div>
						
                    </div>
                    
				</div>
				
                <div class="col-md-9">
					<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php
						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'loop-templates/content', 'search' );
						?>

					<?php endwhile; ?>

					<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>
                
                </div>

            </div>
		</main><!-- #main -->
        

        <div class="row">
            <!-- The pagination component -->
            <div class="col-md-9 offset-md-3 mt-4 text-center">
                <?php linksrecruitment_pagination(); ?>
            </div>

	    </div> <!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>