<?php
/**
 * Template Name: Client
 *
 *
 * @package linksrecruitment
 */

get_header();
$container = get_theme_mod( 'linksrecruitment_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">
<?php if( get_field('banner_image') ) { ?>

	<div class="banner text-center" style="background-image:url('<?php the_field( 'banner_image' ); ?>');">

			<header class="entry-header">

				<h1 class="align-middle"><?php the_field( 'page_title' ); ?> <?php the_field( 'page_title_highlight' ); ?></h1>

			</header><!-- .entry-header -->

	</div>

<?php } else { ?>

	<div class="banner default text-center">

		<header class="entry-header">

			<h1 class="align-middle"><?php the_field( 'page_title' ); ?> <?php the_field( 'page_title_highlight' ); ?></h1>

		</header><!-- .entry-header -->

	</div>

<?php } ?>

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="candidate-section">
			<div class="row">
				<div class="col-md-6">
                    <h2 class="text-center"><?php the_field( 'section_title' ); ?></h2>
					<?php the_field( 'section_content' ); ?>
				</div>
				<div class="col-md-6">
				<?php
                // Get the Video Fields
				$video =  get_field('section_video'); // MP4 Field Name
				$video_poster  = get_field('poster_image'); // Poster Image Field Name
                // Build the  Shortcode
                $attr =  array(
				'mp4'      => $video,
				'poster'   => $video_poster,
                'preload'  => 'auto',
                'autoplay'  => 'on',
                'loop'     => 'on'
                );

                // Display the Shortcode
                echo wp_video_shortcode(  $attr );
                ?>
				</div>
			</div>
		</div>
		

    </div><!-- Container end -->
    
    <section class="testimonials bg-primary">
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-3">
					<div id="testimonial-slider" class="carousel slide" data-ride="carousel" data-interval="9000">

						<?php $args = array(
							'post_type' => 'testimonials',
							'posts_per_page' => 5,
							'category_name'  => 'client'
							);
							$quotes = new WP_Query($args);
							if($quotes->have_posts()):
							$count = $quotes->found_posts;
						?>
						<ol class="carousel-indicators">
							<?php for($i = 0; $i < $count ;  $i++) { ?>
									<li data-target="#testimonial-slider" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''?>"></li>
							<?php } ?>
						</ol> <!--.carousel-indicators-->

						<div class="carousel-inner text-center" role="listbox">
							<?php $i = 0; while($quotes->have_posts()): $quotes->the_post(); ?>
								<div class="carousel-item p-3 <?php echo ($i == 0) ? 'active' : ''?>">
									
									<?php the_content(); ?>
									<p><cite class="text-uppercase"><?php the_title(); ?></cite></p>
									
								</div><!--.carousel-item-->
							<?php $i++; endwhile; ?>
						</div> <!--.carouse-inner-->

						<?php endif;  wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

    <section class="why">
        <div class="container">
            <div class="row">
                <?php $sectors = get_field( 'sectors_section' ); ?>
                <div class="col-md-12 section-header text-center">
                    <h2><?php echo $sectors['sectors_title']; ?></h2>
                </div>
                <?php

                // Stop if there's nothing to display.
                if ( ! have_rows( 'sectors_section' ) ) {
                    return false;
                }

                if ( have_rows( 'sectors_section' ) ) : ?>

                    <?php while ( have_rows( 'sectors_section' ) ) : the_row();

                        // Services Sub Repeater.
                        if ( have_rows( 'sectors' ) ) : ?>

                            <?php
                            while ( have_rows( 'sectors' ) ) : the_row();

                                $title = get_sub_field( 'sector_title' );
                                $desc = get_sub_field( 'sector_description' );
                                $link = get_sub_field( 'sector_page_link' );
                            ?>

                                <div class="col-md-4 text-center">

                                    <h3>
                                        <a href="<?php echo $link; ?>"><?php echo $title; ?></a>
                                    </h3>
                                    <p><?php echo $desc; ?></p>

                                </div>

                            <?php endwhile; ?> 
                        

                        <?php endif; ?>

                    <?php endwhile; ?>

                <?php endif; ?>

                <div class="col-md-12 text-center">
                    <p><?php echo $why['why_answer']; ?></p>
                </div>

            </div>
        </div>
    </section>

    <section class="benefits bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="section-title text-center">
                        <?php $benefitsTitle = get_field( 'benefits_section' ); ?>
                        <h2><?php echo $benefitsTitle['benefits_title']; ?></h2>
                    </div>
                    <div class="answers text-center">
                        <?php

                        // Stop if there's nothing to display.
                        if ( ! have_rows( 'benefits_section' ) ) {
                            return false;
                        }

                        if ( have_rows( 'benefits_section' ) ) : ?>

                            <?php while ( have_rows( 'benefits_section' ) ) : the_row();

                                // Benefits Sub Repeater.
                                if ( have_rows( 'benefits' ) ) : ?>

                                    <?php
                                    while ( have_rows( 'benefits' ) ) : the_row();

                                        $benefitTitle = get_sub_field( 'benefit_title' );
                                        $benefitCcontent = get_sub_field( 'benefit_content' );
                                    ?>

                                    <h3><?php echo $benefitTitle; ?></h3>
                                    <p><?php echo $benefitCcontent; ?></p>

                                    <?php endwhile; ?>                                 

                                <?php endif; ?>

                            <?php endwhile; ?>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

	<section class="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
                    <div class="box">
                        <h2><?php the_field( 'contact_title' ); ?></h2>
				        <a href="/contact" class="btn btn-lg btn-primary"><?php the_field( 'contact_button_text' ); ?></a>
                    </div>
				</div>
			</div>
		</div>
	</section>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
