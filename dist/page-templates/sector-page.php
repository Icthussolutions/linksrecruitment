<?php
/**
 * Template Name: Sector
 *
 *
 * @package linksrecruitment
 */

get_header();
$container = get_theme_mod( 'linksrecruitment_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">
<?php if( get_field('banner_image') ) { ?>

	<div class="banner text-center" style="background-image: url('<?php the_field( 'banner_image' ); ?>');">

			<header class="entry-header">

				<h1 class="align-middle"><?php the_field( 'page_title' ); ?> <?php the_field( 'page_title_highlight' ); ?></h1>

				<?php 
				if ( get_field( 'button_url' ) ) {
				printf( '<a class="btn btn-lg btn-primary" href="%1$s">View Latest %2$s Jobs</a>', get_category_link( get_field( 'button_url' ) ), single_cat_title( __( '', 'linksrecruitment' ) ) ); 
				}
				?>

			</header><!-- .entry-header -->

	</div>

<?php } else { ?>

	<div class="banner default text-center">

		<header class="entry-header">

			<h1 class="align-middle"><?php the_field( 'page_title' ); ?> <span><?php the_field( 'page_title_highlight' ); ?></span></h1>

		</header><!-- .entry-header -->

	</div>

<?php } ?>

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="sector-section">
			<div class="row">
				<div class="col-md-6">
                    <?php if (the_field( 'sector_title' )) { ?>
                    <h2 class="text-center"><?php the_field( 'sector_title' ); ?></h2>
                    <?php } ?>
					<?php the_field( 'sector_content' ); ?>
				</div>
				<div class="col-md-6">
				<?php
                // Get the Video Fields
				$video =  get_field('sector_video'); // MP4 Field Name
				$video_poster  = get_field('poster_image'); // Poster Image Field Name
                // Build the  Shortcode
                $attr =  array(
				'mp4'      => $video,
				'poster'   => $video_poster,
                'preload'  => 'auto',
                'autoplay'  => 'on',
				'loop'     => 'on',
				'muted'		=> '1'
                );

                // Display the Shortcode
                echo wp_video_shortcode(  $attr );
                ?>
				</div>
			</div>
		</div>

		<div class="row">

			<main class="site-main col-md-10 offset-md-1 text-center" id="main">

				

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'sector' ); ?>


				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		</div><!-- .row -->
		

	</div><!-- Container end -->
	
	<section class="sector-outline bg-primary">
        <div class="container">
            <div class="row">
				<?php 
				
				$outline = get_field( 'sector_outline' );
				$jobs1 = $outline['job_types'];
				$jobs2 = $outline['extra_job_types'];
				
				
				?>
                <div class="col-md-10 offset-md-1 section-header text-center">
                    <h2><?php echo $outline['outline_title']; ?></h2>
                    <p class="lead"><?php echo $outline['outline_content_1']; ?></p>
                </div>
                <?php

                // Stop if there's nothing to display.
                if ( ! have_rows( 'sector_outline' ) ) {
                    return false;
                }

                if ( have_rows( 'sector_outline' ) ) : ?>

                    <?php while ( have_rows( 'sector_outline' ) ) : the_row(); ?>

						<?php if (!$jobs2) : ?>

							<div class="col-md-10 offset-md-1 text-center">
								<?php echo $jobs1; ?>
								<p><?php echo $outline['outline_content_2']; ?></p>
							</div>

						<?php else : ?>

							<div class="col-md-5 offset-md-1 text-center">
								<?php echo $jobs1; ?>
							</div>
							<div class="col-md-5 text-center">
								<?php echo $jobs2; ?>
							</div>
							<div class="col-md-10 offset-md-1 text-center">
								<p><?php echo $outline['outline_content_2']; ?></p>
							</div>

						<?php endif; ?>

                    <?php endwhile; ?>

				<?php endif; ?>
				
            </div>
        </div>
	</section>
	
	<section class="aims">
        <div class="container">
            <div class="row">
                <?php $aims = get_field( 'sector_aims' ); ?>
                <div class="col-md-12 section-header text-center">
                    <h2><?php echo $aims['aims_title']; ?></h2>
                    <p class="lead"><?php echo $aims['aims_subtitle']; ?></p>
				</div>
				<div class="col-md-10 offset-md-1 text-center">
                <?php

                // Stop if there's nothing to display.
                if ( ! have_rows( 'sector_aims' ) ) {
                    return false;
                }

                if ( have_rows( 'sector_aims' ) ) : ?>

                    <?php while ( have_rows( 'sector_aims' ) ) : the_row();

                        // Services Sub Repeater.
                        if ( have_rows( 'aims_list' ) ) : ?>

                            <?php
                            while ( have_rows( 'aims_list' ) ) : the_row();

                                $aim = get_sub_field( 'list_content' );
                            ?>

                                <?php echo $aim; ?>



                            <?php endwhile; ?> 
                        

                        <?php endif; ?>

                    <?php endwhile; ?>

                <?php endif; ?>

                </div>

            </div>
        </div>
    </section>
    
	<section class="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="box">
                        <h2><?php the_field( 'contact_title' ); ?></h2>
				        <a href="/contact" class="btn btn-lg btn-primary"><?php the_field( 'contact_button_text' ); ?></a>
                    </div>
				</div>
			</div>
		</div>
	</section>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
