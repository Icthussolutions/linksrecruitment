<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package linksrecruitment
 */

get_header();

$container   = get_theme_mod( 'linksrecruitment_container_type' );
$sidebar_pos = get_theme_mod( 'linksrecruitment_sidebar_position' );

?>

<div class="wrapper" id="error-404-wrapper">

	<div class="banner jobs bg-primary text-center">
	<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'linksrecruitment' ); ?></h1>
	<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'linksrecruitment' ); ?></p>
	</div>

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<div class="col-md-12 mt-4 mb-4 content-area" id="primary">

				<main class="site-main" id="main">

					<section class="error-404 not-found">


						<div class="row">

							<div class="col-md-4">
							<?php
								if(is_active_sidebar('error404-1')){
								dynamic_sidebar('error404-1');
								}
							?>
							</div>

							<div class="col-md-4">
							<?php
								if(is_active_sidebar('error404-2')){
								dynamic_sidebar('error404-2');
								}
							?>
							</div>

							<div class="col-md-4">
							<?php
								if(is_active_sidebar('error404-3')){
								dynamic_sidebar('error404-3');
								}
							?>
							</div>

						</div><!-- .row -->

					</section><!-- .error-404 -->

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
