<?php
/**
 * 
 * Template Name: Home Page
 *
 * @package linksrecruitment
 */

get_header();

$container   = get_theme_mod( 'linksrecruitment_container_type' );

?>

<div class="wrapper" id="page-wrapper">

	<div id="main-slider" class="carousel slide" data-ride="carousel" data-interval="5000">

		<?php $args = array(
			'post_type' => 'carousel',
			'posts_per_page' => 7,
			'orderby' => 'date',
			'order' => 'ASC'
			);
			$slider = new WP_Query($args);
			if($slider->have_posts()):
			$count = $slider->found_posts;
		?>
		<ol class="carousel-indicators d-none d-md-flex">
			<?php for($i = 0; $i < $count ;  $i++) { ?>
					<li data-target="#main-slider" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''?>"></li>
			<?php } ?>
		</ol> <!--.carousel-indicators-->

		<div class="carousel-inner" role="listbox">
			<?php $i = 0; while($slider->have_posts()): $slider->the_post(); ?>
				<div class="carousel-item <?php echo ($i == 0) ? 'active' : ''?>">
						<?php the_post_thumbnail( 'slider', array(
																'class' => 'd-block img-fluid',
																'alt' => get_the_title() ) ) ; ?>
						<?php if ( get_field('sector_link') ) { ?>
						<div class="carousel-caption">
							<h2 class="text-uppercase"><a href="<?php echo get_category_link( get_field( 'sector_jobs_link' ) ); ?>"><?php echo get_the_title(); ?></a></h2>
							<p class="text-center">
							<?php printf( '<a class="btn btn-lg btn-primary mr-3" href="%1$s">View Latest Jobs</a>', get_category_link( get_field( 'sector_jobs_link' ) ) ); ?>
							<?php printf( '<a class="btn btn-lg btn-secondary ml-3" href="%1$s">Learn More</a>', get_field('sector_link') ); ?>
							</p>
						</div>
						<?php } ?>
				</div><!--.carousel-item-->
			<?php $i++; endwhile; ?>
		</div> <!--.carouse-inner-->


		<a href="#main-slider" class="carousel-control-prev" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a href="#main-slider" class="carousel-control-next" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>

		<?php endif;  wp_reset_postdata(); ?>
	</div>

	<div class="text-center <?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main col-md-10 offset-md-1" id="main">

				

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'page' ); ?>


				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		</div><!-- .row -->
		<section class="sectors">
			<div class="section-header">
				<h2><?php the_field('section_title'); ?></h2>
			</div>					
		
			<div class="row sectors-container">

			<?php

				// check if the repeater field has rows of data
				if( have_rows( 'sectors' ) ):

					// loop through the rows of data
					while ( have_rows( 'sectors' ) ) : the_row(); ?>

					<div class="sector-box col-10 col-md-4 mb-5">
					
						<div class="sector">
						
							<?php
							$image = get_sub_field( 'sector_image' );
							$size = 'large'; // (thumbnail, medium, large, full or custom size)
							
							if( $image ) {
							
								echo wp_get_attachment_image( $image, $size );
							
							}
							?>

							<div class="overlay">
								<a href="<?php the_sub_field( 'sector_page_link' ); ?>"><i class="fa fa-arrow-circle-right"></i></a>
							<h3><?php the_sub_field( 'sector_heading' ); ?></h3>
						</div>

						</div>

						

					</div>

					<?php endwhile;

				else :

					// no rows found

				endif;

			?>

			</div>

		</section>

	

	</div><!-- Container end -->

	<section class="testimonials bg-primary">
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-3">
					<div id="testimonial-slider" class="carousel slide" data-ride="carousel" data-interval="9000">

						<?php $args = array(
							'post_type' => 'testimonials',
							'posts_per_page' => 5,
							'category_name'  => 'client'
							);
							$quotes = new WP_Query($args);
							if($quotes->have_posts()):
							$count = $quotes->found_posts;
						?>
						<ol class="carousel-indicators">
							<?php for($i = 0; $i < $count ;  $i++) { ?>
									<li data-target="#testimonial-slider" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''?>"></li>
							<?php } ?>
						</ol> <!--.carousel-indicators-->

						<div class="carousel-inner text-center" role="listbox">
							<?php $i = 0; while($quotes->have_posts()): $quotes->the_post(); ?>
								<div class="carousel-item p-3 <?php echo ($i == 0) ? 'active' : ''?>">
									
									<?php the_content(); ?>
									<p><cite class="text-uppercase"><?php the_title(); ?></cite></p>
									
								</div><!--.carousel-item-->
							<?php $i++; endwhile; ?>
						</div> <!--.carouse-inner-->

						<?php endif;  wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="job-links">
		<div class="section-header">
			<h2><?php the_field('latest_jobs_title'); ?></h2>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-3">
				<?php 

				$terms = get_terms('industry'); 
				if ( !empty( $terms ) && !is_wp_error( $terms ) ){ 
				echo '<ul class="list-group">'; 

				foreach ( $terms as $term ) { 
				$term = sanitize_term( $term, 'industry' ); 
				$term_link = get_term_link( $term, 'industry' ); 

					echo '<li class="list-group-item d-flex justify-content-between align-items-center"><a href="' . esc_url( $term_link ) . '">Latest ' . $term->name . ' Jobs</a><span class="badge badge-primary badge-pill">' . $term->count . '</span></li>'; 
				} 
				echo '</ul>';
				}

				?> 
				</div>
			</div>
		</div>
	</section>
	<section class="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="box">
						<h2><?php the_field( 'contact_title' ); ?></h2>
						<a href="/contact" class="btn btn-lg btn-primary"><?php the_field( 'contact_button_text' ); ?></a>
					</div>
				</div>
			</div>
		</div>
	</section>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
