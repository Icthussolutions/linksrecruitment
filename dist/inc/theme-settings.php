<?php
/**
 * Check and setup theme's default settings
 *
 * @package linksrecruitment
 *
 */

if ( ! function_exists ( 'linksrecruitment_setup_theme_default_settings' ) ) {
	function linksrecruitment_setup_theme_default_settings() {

		// check if settings are set, if not set defaults.
		// Caution: DO NOT check existence using === always check with == .
		// Latest blog posts style.
		$linksrecruitment_posts_index_style = get_theme_mod( 'linksrecruitment_posts_index_style' );
		if ( '' == $linksrecruitment_posts_index_style ) {
			set_theme_mod( 'linksrecruitment_posts_index_style', 'default' );
		}

		// Sidebar position.
		$linksrecruitment_sidebar_position = get_theme_mod( 'linksrecruitment_sidebar_position' );
		if ( '' == $linksrecruitment_sidebar_position ) {
			set_theme_mod( 'linksrecruitment_sidebar_position', 'right' );
		}

		// Container width.
		$linksrecruitment_container_type = get_theme_mod( 'linksrecruitment_container_type' );
		if ( '' == $linksrecruitment_container_type ) {
			set_theme_mod( 'linksrecruitment_container_type', 'container' );
		}
	}
}

// CHANGE EXCERPT LENGTH FOR DIFFERENT POST TYPES
 
function linksrecruitment_custom_excerpt_length($length) {
	global $post;
	if ($post->post_type == 'post')
	return 32;
	else if ($post->post_type == 'jobs')
	return 45;
}
add_filter('excerpt_length', 'linksrecruitment_custom_excerpt_length');