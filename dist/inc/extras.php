<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package linksrecruitment
 */

add_filter( 'body_class', 'linksrecruitment_body_classes' );

if ( ! function_exists( 'linksrecruitment_body_classes' ) ) {
	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 *
	 * @return array
	 */
	function linksrecruitment_body_classes( $classes ) {
		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}
}

// Removes tag class from the body_class array to avoid Bootstrap markup styling issues.
add_filter( 'body_class', 'linksrecruitment_adjust_body_class' );

if ( ! function_exists( 'linksrecruitment_adjust_body_class' ) ) {
	/**
	 * Setup body classes.
	 *
	 * @param string $classes CSS classes.
	 *
	 * @return mixed
	 */
	function linksrecruitment_adjust_body_class( $classes ) {

		foreach ( $classes as $key => $value ) {
			if ( 'tag' == $value ) {
				unset( $classes[ $key ] );
			}
		}

		return $classes;

	}
}

// Filter custom logo with correct classes.
add_filter( 'get_custom_logo', 'linksrecruitment_change_logo_class' );

if ( ! function_exists( 'linksrecruitment_change_logo_class' ) ) {
	/**
	 * Replaces logo CSS class.
	 *
	 * @param string $html Markup.
	 *
	 * @return mixed
	 */
	function linksrecruitment_change_logo_class( $html ) {

		$html = str_replace( 'class="custom-logo"', 'class="img-fluid"', $html );
		$html = str_replace( 'class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html );
		$html = str_replace( 'alt=""', 'title="Home" alt="logo"' , $html );

		return $html;
	}
}

/**
 * Display navigation to next/previous post when applicable.
 */

if ( ! function_exists ( 'linksrecruitment_post_nav' ) ) {
	function linksrecruitment_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
				<nav class="container navigation post-navigation">
					<h2 class="sr-only"><?php _e( 'Post navigation', 'linksrecruitment' ); ?></h2>
					<div class="row nav-links justify-content-between">
						<?php

							if ( get_previous_post_link() ) {
								previous_post_link( '<span class="nav-previous">%link</span>', _x( '<i class="fa fa-angle-left"></i>&nbsp;%title', 'Previous post link', 'linksrecruitment' ) );
							}
							if ( get_next_post_link() ) {
								next_post_link( '<span class="nav-next">%link</span>',     _x( '%title&nbsp;<i class="fa fa-angle-right"></i>', 'Next post link', 'linksrecruitment' ) );
							}
						?>
					</div><!-- .nav-links -->
				</nav><!-- .navigation -->

		<?php
	}
}

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyDCfhdMHdvA25Hr4kWGLDvD0I8L0MTni-Y');
}

add_action('acf/init', 'my_acf_init');

/**
 * Primary Menu Extras
 *
 */
function linksrecruitment_primary_menu_extras( $menu, $args ) {

	if( 'primary' == $args->theme_location )
		$menu .= '<li class="menu-item search"><a href="#" class="search-toggle"><i class="fa fa-search"></i></a>' . get_search_form( false ) . '</li>';

	return $menu;
}
add_filter( 'wp_nav_menu_items', 'linksrecruitment_primary_menu_extras', 10, 2 );

function hide_menu() {
	global $current_user;
	$user_id = get_current_user_id();
	// echo "user:".$user_id;   // Use this to find your user id quickly
   
	   if(is_admin() && !$user_id == '1'){

		add_filter('acf/settings/show_admin', '__return_false');
   
   
		// To remove the theme editor and theme options submenus from
		// the Appearance admin menu, as well as the main 'Themes'
		// submenu you would use 
	}

}

add_action('admin_head', 'hide_menu');

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );


add_shortcode( 'video', function ( $atts, $content ) 
{
    $output = wp_video_shortcode( $atts, $content );

    if( ! isset( $atts['muted'] ) || ! wp_validate_boolean( $atts['muted'] ) ) 
        return $output;

    if( false !== stripos( $output, ' muted="1"' ) )
        return $output;

    return str_ireplace( '<video ', '<video muted="1" ', $output ); 
} );