<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package linksrecruitment
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'linksrecruitment_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>
<footer class="footer-bottom">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6">
			<?php
				if(is_active_sidebar('contactinfo')){
				dynamic_sidebar('contactinfo');
				}
			?>
			</div>
			<div class="col-lg-3 col-md-6">
			<?php
				if(is_active_sidebar('headoffice')){
				dynamic_sidebar('headoffice');
				}
			?>
			</div>
			<div class="col-lg-2 col-md-3">
			<?php
				if(is_active_sidebar('socialicons')){
				dynamic_sidebar('socialicons');
				}
			?>
			</div>
			<div class="col-lg-2 col-md-3">
			<?php
				if(is_active_sidebar('legalstuff')){
				dynamic_sidebar('legalstuff');
				}
			?>
			</div>
			<div class="col-6 col-lg-2 col-md-6">
			<?php
				if(is_active_sidebar('credits')){
				dynamic_sidebar('credits');
				}
			?>
			</div>
		</div>
	</div>
</footer>
<div class="wrapper footer-main bg-primary" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">

					<?php wp_nav_menu(
					array(
						'theme_location'  => 'footer',
						'menu_class'      => 'footer-menu d-md-flex justify-content-md-around',
					)
				); ?>
					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

