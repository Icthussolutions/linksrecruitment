<?php

if ( ! function_exists( 'linksrecruitment_pagination' ) ) :

	/**
	 * Displays the navigation to next/previous set of posts, when applicable.
	 */
	function linksrecruitment_pagination() {
		// Don't print empty markup if there's only one page.
		if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
			return;
		} ?>
		<ul class="pagination">
			<?php if ( get_next_posts_link() ) : ?>
				<li class="page-link"><?php next_posts_link( esc_html__( 'Older jobs', 'linksrecruitment' ) ); ?></li>
			<?php endif; ?>
			<?php if ( get_previous_posts_link() ) : ?>
				<li class="page-link"><?php previous_posts_link( esc_html__( 'Newer jobs', 'linksrecruitment' ) ); ?> </li>
			<?php endif; ?>
		</ul>
		<?php

	}
endif;