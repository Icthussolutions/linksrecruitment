<?php
/**
 * Declaring widgets
 *
 * @package linksrecruitment
 */

/**
 * Count number of widgets in a sidebar
 * Used to add classes to widget areas so widgets can be displayed one, two, three or four per row
 */
if ( ! function_exists( 'linksrecruitment_slbd_count_widgets' ) ) {
	function linksrecruitment_slbd_count_widgets( $sidebar_id ) {
		// If loading from front page, consult $_wp_sidebars_widgets rather than options
		// to see if wp_convert_widget_settings() has made manipulations in memory.
		global $_wp_sidebars_widgets;
		if ( empty( $_wp_sidebars_widgets ) ) :
			$_wp_sidebars_widgets = get_option( 'sidebars_widgets', array() );
		endif;

		$sidebars_widgets_count = $_wp_sidebars_widgets;

		if ( isset( $sidebars_widgets_count[ $sidebar_id ] ) ) :
			$widget_count = count( $sidebars_widgets_count[ $sidebar_id ] );
			$widget_classes = 'widget-count-' . count( $sidebars_widgets_count[ $sidebar_id ] );
			if ( $widget_count % 4 == 0 || $widget_count > 6 ) :
				// Four widgets per row if there are exactly four or more than six
				$widget_classes .= ' col-md-3';
			elseif ( 6 == $widget_count ) :
				// If two widgets are published
				$widget_classes .= ' col-md-2';
			elseif ( $widget_count >= 3 ) :
				// Three widgets per row if there's three or more widgets 
				$widget_classes .= ' col-md-4';
			elseif ( 2 == $widget_count ) :
				// If two widgets are published
				$widget_classes .= ' col-md-12';
			elseif ( 1 == $widget_count ) :
				// If just on widget is active
				$widget_classes .= ' col-md-12';
			endif; 
			return $widget_classes;
		endif;
	}
}

add_action( 'widgets_init', 'linksrecruitment_widgets_init' );

if ( ! function_exists( 'linksrecruitment_widgets_init' ) ) {
	/**
	 * Initializes themes widgets.
	 */
	function linksrecruitment_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Right Sidebar', 'linksrecruitment' ),
			'id'            => 'right-sidebar',
			'description'   => 'Right sidebar widget area',
			'before_widget' => '<aside id="%1$s" class="card %2$s">',
			'after_widget'  => '</div></aside>',
			'before_title'  => '<div class="card-header text-white bg-primary"><h4>',
			'after_title'   => '</h4></div><div class="card-body">',
		) );

		register_sidebar( array(
			'name'          => __( 'Left Sidebar', 'linksrecruitment' ),
			'id'            => 'left-sidebar',
			'description'   => 'Left sidebar widget area',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Top Full', 'linksrecruitment' ),
			'id'            => 'statichero',
			'description'   => 'Full top widget with dynmic grid',
		    'before_widget'  => '<div id="%1$s" class="static-hero-widget %2$s '. linksrecruitment_slbd_count_widgets( 'statichero' ) .'">', 
		    'after_widget'   => '</div><!-- .static-hero-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Footer Full', 'linksrecruitment' ),
			'id'            => 'footerfull',
			'description'   => 'Full sized footer widget with dynamic grid',
		    'before_widget'  => '<div id="%1$s" class="footer-widget %2$s '. linksrecruitment_slbd_count_widgets( 'footerfull' ) .'">', 
		    'after_widget'   => '</div><!-- .footer-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>', 
		) );

		register_sidebar( array(
			'name'          => __( 'Contact Info', 'linksrecruitment' ),
			'id'            => 'contactinfo',
			'description'   => 'Footer widget to display contact number etc',
		    'before_widget'  => '<div id="%1$s" class="footer-widget %2$s '. linksrecruitment_slbd_count_widgets( 'contactinfo' ) .'">', 
		    'after_widget'   => '</div><!-- .footer-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>', 
		) );

		register_sidebar( array(
			'name'          => __( 'Head Office', 'linksrecruitment' ),
			'id'            => 'headoffice',
			'description'   => 'Footer widget to display head office details',
		    'before_widget'  => '<div id="%1$s" class="footer-widget %2$s '. linksrecruitment_slbd_count_widgets( 'headoffice' ) .'">', 
		    'after_widget'   => '</div><!-- .footer-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>', 
		) );

		register_sidebar( array(
			'name'          => __( 'Social Icons', 'linksrecruitment' ),
			'id'            => 'socialicons',
			'description'   => 'Footer widget to display Social Icons',
		    'before_widget'  => '<div id="%1$s" class="footer-widget %2$s '. linksrecruitment_slbd_count_widgets( 'socialicons' ) .'">', 
		    'after_widget'   => '</div><!-- .footer-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>', 
		) );

		register_sidebar( array(
			'name'          => __( 'Legal Stuff', 'linksrecruitment' ),
			'id'            => 'legalstuff',
			'description'   => 'Footer widget to display legal stuff',
		    'before_widget'  => '<div id="%1$s" class="footer-widget %2$s '. linksrecruitment_slbd_count_widgets( 'legalstuff' ) .'">', 
		    'after_widget'   => '</div><!-- .footer-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>', 
		) );

		register_sidebar( array(
			'name'          => __( 'Accreditations', 'linksrecruitment' ),
			'id'            => 'credits',
			'description'   => 'Footer widget to display Accreditations',
		    'before_widget'  => '<div id="%1$s" class="footer-widget %2$s '. linksrecruitment_slbd_count_widgets( 'credits' ) .'">', 
		    'after_widget'   => '</div><!-- .footer-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>', 
		) );

		register_sidebar( array(
			'name'          => __( 'Error 404', 'linksrecruitment' ),
			'id'            => 'error404-1',
			'description'   => '404 page widget to display whole site search',
		    'before_widget'  => '<div id="%1$s" class="widget %2$s '. linksrecruitment_slbd_count_widgets( 'error404-1' ) .'">', 
		    'after_widget'   => '</div><!-- .footer-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>', 
		) );

		register_sidebar( array(
			'name'          => __( 'Error 404b', 'linksrecruitment' ),
			'id'            => 'error404-2',
			'description'   => '404 page widget to display jobs search',
		    'before_widget'  => '<div id="%1$s" class="widget %2$s '. linksrecruitment_slbd_count_widgets( 'error404-2' ) .'">', 
		    'after_widget'   => '</div><!-- .footer-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>', 
		) );

		register_sidebar( array(
			'name'          => __( 'Error 404c', 'linksrecruitment' ),
			'id'            => 'error404-3',
			'description'   => '404 page widget to display latest jobs',
		    'before_widget'  => '<div id="%1$s" class="widget %2$s '. linksrecruitment_slbd_count_widgets( 'error404-3' ) .'">', 
		    'after_widget'   => '</div><!-- .footer-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>', 
		) );

	}
} // endif function_exists( 'linksrecruitment_widgets_init' ).