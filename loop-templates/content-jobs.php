<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package linksrecruitment
 */

?>


    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

        <header class="entry-header">

            <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
            '</a></h2>' ); ?>

            <?php if ( 'jobs' == get_post_type() ) : ?>

                <ul class="entry-meta d-none d-md-block">
                <?php
                    echo get_the_term_list( $post->ID, 'job_type', '<li>', ',', '</li>' );
                    echo get_the_term_list( $post->ID, 'job_hours', '<li>', ',', '</li>' );
                    echo get_the_term_list( $post->ID, 'industry', '<li>', ',', '</li>' );
                    echo get_the_term_list( $post->ID, 'job_location', '<li>', ',', '</li>' );
                ?>
                </ul><!-- .entry-meta -->

            <?php endif; ?>

        </header><!-- .entry-header -->

        <div class="entry-content">

            <?php
            the_excerpt();
            ?>

        </div><!-- .entry-content -->

    </article><!-- #post-## -->
